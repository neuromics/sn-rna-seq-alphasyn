#!/bin/bash


sample=$1
if [ "$sample" == "" ]; then
    echo "ERROR: Supply sample id (e.g. PFCTX019)"
    exit 2
fi

hd=$2
if [ "$hd" == "" ]; then
    echo "ERROR: Supply mounted hd (e.g. /data1)"
    exit 3
fi

out_dir="/fileshares/forneurosysmedresults/scrna-big-project"

data_dir=($(find /fileshares/forneurosysmedrawdata/scrna-big-project/*/raw_data/${sample} -type d) )
data_dir=$(printf ",%s" "${data_dir[@]}")
data_dir=${data_dir:1}
echo
echo ">>> SAMPLE: $sample"
echo ">>> TMP DIR: $hd"
echo ">>> DATA DIR: $data_dir"

sample_ids=($(find /fileshares/forneurosysmedrawdata/scrna-big-project/*/raw_data/${sample} -type f -name *_R1_*.fastq.gz -printf "%f\n" | sed 's/_S.\+_L.\+_R1_.\+\.fastq\.gz//') )
sample_ids=$(printf ",%s" "${sample_ids[@]}")
sample_ids=${sample_ids:1}

echo
echo ">>> SAMPLES: ${sample_ids}"

if [ -s ${out_dir}/results/${sample}/${sample}/outs/raw_feature_bc_matrix.h5 ]; then
    echo
    echo "ERROR: Seems that the ouput file exists already!"
    exit 4
fi
if [ -s ${out_dir}/results/${sample}/outs/raw_feature_bc_matrix.h5 ]; then
    echo
    echo "ERROR: Seems that the ouput file exists already!"
    exit 4
fi


# R1=`find ${data_dir} -name *_R1_001.fastq.gz | sort`
# R2=`find ${data_dir} -name *_R2_001.fastq.gz | sort`
# echo "R1 files:"
# echo $R1
# echo "R2 files:"
# echo $R2

# RUN CELL RANGER
export PATH=/home/gsnido/cellranger-6.1.2/:$PATH
tmp_dir=$(mktemp -d -t tmp_XXXXXXXXXX --tmpdir=${hd})
cur_dir=$(pwd)
cd ${tmp_dir}
echo
echo "###############"
echo "STARTED:"
date
echo "###############"

# echo "Copying fastqfiles..."
# cat ${R1[*]} > ${tmp_dir}/Sample_S1_L001_R1_001.fastq.gz
# cat ${R2[*]} > ${tmp_dir}/Sample_S1_L001_R2_001.fastq.gz

mkdir -p ${out_dir}/results/${sample}

cellranger count \
        --id=${sample} \
        --sample=${sample_ids} \
        --nosecondary \
        --include-introns \
        --fastqs=${data_dir} \
        --transcriptome=/fileshares/forneurosysmedresults/scrna-big-project/sc-rna-seq-azure/ref/GRCh38 > \
        ${out_dir}/results/${sample}/cellranger.stdout 2> \
        ${out_dir}/results/${sample}/cellranger.stderr


echo
echo ">>> COPYING"
echo "    cp -r ${tmp_dir}/${sample} ${out_dir}/results/${sample}/"
cp -r ${tmp_dir}/${sample} ${out_dir}/results/${sample}/ 2> /dev/null
cd ${cur_dir}
rm -rf ${tmp_dir}

echo
echo "###############"
echo "FINISHED:"
date
echo "###############"

