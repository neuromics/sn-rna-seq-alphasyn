R version 4.3.1 (2023-06-16)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 18.04.6 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/openblas/libblas.so.3 
LAPACK: /usr/lib/x86_64-linux-gnu/libopenblasp-r0.2.20.so;  LAPACK version 3.7.1

locale:
 [1] LC_CTYPE=en_DK.UTF-8       LC_NUMERIC=C               LC_TIME=en_GB.UTF-8        LC_COLLATE=en_DK.UTF-8     LC_MONETARY=en_GB.UTF-8    LC_MESSAGES=en_DK.UTF-8    LC_PAPER=en_GB.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C             LC_MEASUREMENT=en_GB.UTF-8 LC_IDENTIFICATION=C       

time zone: Europe/Oslo
tzcode source: system (glibc)

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] KernSmooth_2.23-22  fields_15.2         viridisLite_0.4.2   spam_2.9-1          DoubletFinder_2.0.3 clustree_0.5.0      ggraph_2.1.0        hdf5r_1.3.8         SeuratObject_4.1.4  Seurat_4.4.0        lubridate_1.9.3    
[12] forcats_1.0.0       stringr_1.5.0       dplyr_1.1.3         purrr_1.0.2         readr_2.1.4         tidyr_1.3.0         tibble_3.2.1        ggplot2_3.4.3       tidyverse_2.0.0     nvimcom_0.9-83     

loaded via a namespace (and not attached):
  [1] RColorBrewer_1.1-3     jsonlite_1.8.7         magrittr_2.0.3         spatstat.utils_3.0-3   farver_2.1.1           vctrs_0.6.3            ROCR_1.0-11            spatstat.explore_3.2-3 htmltools_0.5.6.1     
 [10] sctransform_0.4.0      parallelly_1.36.0      htmlwidgets_1.6.2      ica_1.0-3              plyr_1.8.9             plotly_4.10.2          zoo_1.8-12             igraph_1.5.1           mime_0.12             
 [19] lifecycle_1.0.3        pkgconfig_2.0.3        Matrix_1.6-1.1         R6_2.5.1               fastmap_1.1.1          fitdistrplus_1.1-11    future_1.33.0          shiny_1.7.5            digest_0.6.33         
 [28] colorspace_2.1-0       patchwork_1.1.3        tensor_1.5             irlba_2.3.5.1          labeling_0.4.3         progressr_0.14.0       fansi_1.0.5            spatstat.sparse_3.0-2  timechange_0.2.0      
 [37] httr_1.4.7             polyclip_1.10-6        abind_1.4-5            compiler_4.3.1         bit64_4.0.5            withr_2.5.1            backports_1.4.1        carData_3.0-5          viridis_0.6.4         
 [46] maps_3.4.1             ggforce_0.4.1          MASS_7.3-60            tools_4.3.1            lmtest_0.9-40          httpuv_1.6.11          future.apply_1.11.0    goftest_1.2-3          glue_1.6.2            
 [55] nlme_3.1-163           promises_1.2.1         grid_4.3.1             checkmate_2.2.0        Rtsne_0.16             cluster_2.1.4          reshape2_1.4.4         generics_0.1.3         gtable_0.3.4          
 [64] spatstat.data_3.0-1    tzdb_0.4.0             data.table_1.14.8      hms_1.1.3              car_3.1-2              tidygraph_1.2.3        sp_2.1-0               utf8_1.2.3             spatstat.geom_3.2-5   
 [73] RcppAnnoy_0.0.21       ggrepel_0.9.3          RANN_2.6.1             pillar_1.9.0           vroom_1.6.4            later_1.3.1            splines_4.3.1          tweenr_2.0.2           lattice_0.21-9        
 [82] survival_3.5-7         bit_4.0.5              deldir_1.0-9           tidyselect_1.2.0       miniUI_0.1.1.1         pbapply_1.7-2          gridExtra_2.3          scattermore_1.2        graphlayouts_1.0.1    
 [91] matrixStats_1.0.0      pheatmap_1.0.12        stringi_1.7.12         lazyeval_0.2.2         codetools_0.2-19       cli_3.6.1              uwot_0.1.16            xtable_1.8-4           reticulate_1.32.0     
[100] munsell_0.5.0          Rcpp_1.0.11            globals_0.16.2         spatstat.random_3.1-6  png_0.1-8              parallel_4.3.1         ellipsis_0.3.2         dotCall64_1.0-2        listenv_0.9.0         
[109] scales_1.2.1           ggridges_0.5.4         leiden_0.4.3           crayon_1.5.2           rlang_1.1.1            cowplot_1.1.1         
