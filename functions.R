require("parallel")

### FUNCTIONS

# Colorblind-friendly colours
cb_red <- "#DC3220"
cb_blue <- "#005AB5"

make_matrix <- function(df){
    my_matrix <- as.matrix(df[-1])
    rownames(my_matrix) <- c(df[[1]])
    my_matrix
}

# Function to plot a barplot with the number of significant genes

plotHeatmap <- function(dexData, Diagnosis, min_coef, min_p, Cap = 2, Genes = NULL, Reorder = TRUE, genenames.side = "right") {
    row.annot.tmp <- dexData %>% filter(diagnosis == Diagnosis, p_adj < min_p, abs(coef) > min_coef, cluster %in% rownames(ct.order)) %>%
        select(gene_id, cluster, p_adj) %>% 
        group_by(gene_id) %>% 
        arrange(p_adj) %>% 
        slice_head(n = 1) %>% ungroup()
    if (!is.null(Genes)) {
        which.sig.tmp <- Genes
    } else {
        which.sig.tmp <- pull(row.annot.tmp, gene_id)
    }


    row.annot.df.tmp <- as.data.frame(select(row.annot.tmp, cluster) %>% left_join(as_tibble(ct.order, rownames = "cluster"), by = "cluster"))
    #all(row.annot.tmp$cluster == row.annot.df.tmp$cluster)
    rownames(row.annot.df.tmp) <- row.annot.tmp$gene_id

    cb_red <- "#DC3220"
    cb_blue <- "#005AB5"

    coefs <- lapply(c(rownames(ct.order), "bulk"), function(Cluster) {
        res.tmp <- dexData %>% filter(gene_id %in% which.sig.tmp, cluster == Cluster, diagnosis == Diagnosis) %>%
            select(gene_id, coef)
        colnames(res.tmp) <- c("gene_id", Cluster)
        return(res.tmp)
    }) %>% Reduce(function(x, y) full_join(x, y, by = "gene_id"), .) %>% make_matrix
    
    zs <- lapply(c(rownames(ct.order), "bulk"), function(Cluster) {
        res.tmp <- dexData %>% filter(gene_id %in% which.sig.tmp, cluster == Cluster, diagnosis == Diagnosis) %>%
            select(gene_id, z)
        colnames(res.tmp) <- c("gene_id", Cluster)
        return(res.tmp)
    }) %>% Reduce(function(x, y) full_join(x, y, by = "gene_id"), .) %>% make_matrix
    
    pvals <- lapply(c(rownames(ct.order), "bulk"), function(Cluster) {
        res.tmp <- dexData %>% filter(gene_id %in% which.sig.tmp, cluster == Cluster, diagnosis == Diagnosis) %>%
            select(gene_id, pval)
        colnames(res.tmp) <- c("gene_id", Cluster)
        return(res.tmp)
    }) %>% Reduce(function(x, y) full_join(x, y, by = "gene_id"), .) %>% make_matrix
    
    padjs <- lapply(c(rownames(ct.order), "bulk"), function(Cluster) {
        res.tmp <- dexData %>% filter(gene_id %in% which.sig.tmp, cluster == Cluster, diagnosis == Diagnosis) %>%
            select(gene_id, p_adj)
        colnames(res.tmp) <- c("gene_id", Cluster)
        return(res.tmp)
    }) %>% Reduce(function(x, y) full_join(x, y, by = "gene_id"), .) %>% make_matrix
    
    if (Reorder) {
        if (is.null(Genes)) {
            coefs2 <- coefs[match(row.annot.tmp$gene_id, rownames(coefs)),]
            row.order <- lapply(unique(row.annot.df.tmp$ct), function(CT) {
                xsub <- coefs2[row.annot.df.tmp$ct == CT,, drop = FALSE]
                xsub <- xsub[,colnames(xsub) %in% row.annot.df.tmp[row.annot.df.tmp$ct == CT,]$cluster, drop = FALSE]
                xsub <- sign(replace(xsub, is.na(xsub), 0))
                if (nrow(xsub) == 0) {
                    message(paste0("WARNING: No genes found in ", CT))
                    return(NULL)
                } else if (nrow(xsub) <= 2) {
                    message(paste0("WARNING: Few genes (1 or 2) found in ", CT, " (not clustering)"))
                    return(rownames(xsub))
                } else {
                    hc <- hclust(dist(replace(xsub, is.na(xsub), 0)))
                    sv <- svd(t(replace(xsub, is.na(xsub), 0)))$v[,1]
                    hc2 <- as.hclust(reorder(as.dendrogram(hc), wts = sv))
                    return(rownames(xsub)[hc2$order])
                }
            })
            names(row.order) <- unique(row.annot.df.tmp$ct)
            row.order <- do.call("c", row.order[major.ct])
        } else {
            xsub <- sign(replace(coefs, is.na(coefs), 0))
            if(nrow(xsub) == 0) {
                message(paste0("WARNING: No genes found, returning NULL"))
                return(NULL)
            } else if (nrow(xsub) <= 2) {
                message(paste0("WARNING: Few genes (1 or 2) found (not clustering)"))
                row.order <- rownames(xsub)
            } else {
                hc <- hclust(dist(replace(xsub, is.na(xsub), 0)))
                sv <- svd(t(xsub))$v[,1]
                hc2 <- as.hclust(reorder(as.dendrogram(hc), wts = sv))
                row.order <- rownames(xsub)[hc2$order]
            }
            names(row.order) <- rep("ex_00", length(row.order))
        }
    } else {
        if (is.null(Genes)) {
            row.order <- unique(dexData$gene_id)[unique(dexData$gene_id) %in% rownames(coefs)]
            names(row.order) <- rep("ex_00", length(row.order))
        } else {
            row.order <- unique(Genes)
        }
    }
    
    coefs <- coefs[row.order,]
    zs <- zs[row.order,]
    pvals <- pvals[row.order,]
    padjs <- padjs[row.order,]
    
    #zs.sig <- zs[keep.sig.rows,]
    #coefs.sig <- coefs[keep.sig.rows,]
    main_cts <- ct.order[!rownames(ct.order) %in% c("ex", "in", "oligodendrocytes", "astrocytes"),, drop = FALSE]
    ha_c2 <- HeatmapAnnotation(cell_type = factor(c(main_cts$ct, "bulk"), levels = c(major.ct, "bulk")), show_annotation_name = FALSE,
                          col = list(cell_type = setNames(brewer.pal(length(c(major.ct, "bulk")), "Pastel1"), c(major.ct, "bulk"))))
    lgd2 <- Legend(labels = c("FDR < 5%", "p < 0.05"), title = "", 
                   graphics = list(
                                   function(x, y, w, h) grid.points(x, y, gp = gpar(col = "black", fill = "black"), size = unit(2, "mm"), pch = 21),
                                   function(x, y, w, h) grid.points(x, y, gp = gpar(col = "black"), size = unit(1.6, "mm"), pch = 4)))
    Rowsplit <- NULL
    if (Reorder) {Rowsplit <- factor(str_replace(names(row.order), "\\d+$", ""), levels = major.ct)}
    Layerfun <- NULL
    if (Reorder) {
        Layerfun <- function(j, i, x, y, width, height, fill) {
            if(str_replace(names(row.order), "\\d+$", "")[i][1] == c(main_cts$ct, "bulk")[j][1]) {
                grid.rect(gp = gpar(lwd = 2, fill = "transparent"))
                }
        }
    }
    ht2 <- Heatmap(coefs,
            column_title = paste0(Diagnosis), 
            name = paste0("logFC (capped at +/- ", Cap, ")"),
            col = colorRamp2(c(-Cap, 0, Cap), c(cb_blue, "white", cb_red)), # For capping logFC
            na_col = "grey80",
            cluster_rows = FALSE,
            cluster_columns = FALSE,
            rect_gp = gpar(col = "grey40", lwd = 1),
            row_split = Rowsplit,
            row_gap = unit(1, "mm"),
            row_names_side = genenames.side,
            row_names_gp = gpar(fontsize = 8, col = "black"),
            row_title = NULL,
            column_split = factor(c(main_cts$ct, "bulk"), levels = c(major.ct, "bulk")),
            column_names_rot = 45,
            cluster_column_slices = FALSE,
            column_names_gp = gpar(fontsize = 8),
            cell_fun = function(j, i, x, y, width, height, fill) {
                if (!(is.na(padjs[i,j])) & (padjs[i, j] < 0.05))
                    grid.points(x, y, pch = 21, size = unit(2, "mm"), gp = gpar(col = "black", fill = "black"))
                else if (!(is.na(pvals[i,j])) & (pvals[i, j] < 0.05))
                    grid.points(x, y, pch = 4, size = unit(1.6, "mm"))
            },
            layer_fun = Layerfun,
            top_annotation = ha_c2
    )
    return(list(plot = ht2, legend = lgd2, n_genes = nrow(coefs)))
}

# Same as previous, but across cell types instead of across genes

plotBarplot <- function(dexData, Diagnosis, min_coef = 0, min_p = 0.05, which.clusters = rownames(ct.order), which.p = c("fdr", "p"), x.lim = NULL) {
    which.p = which.p[1]
    if (which.p == "fdr") {
        Barplot <- dexData %>% filter(diagnosis == Diagnosis) %>%
            filter(p_adj < min_p, abs(coef) > min_coef)
    } else {
        Barplot <- dexData %>% filter(diagnosis == Diagnosis) %>%
            filter(pval < min_p, abs(coef) > min_coef)
    }
    Barplot <- Barplot %>% mutate(direction = ifelse(coef < 0, "downregulated", "upregulated")) %>%
        filter(cluster %in% which.clusters) %>%
        select(gene_id, cluster, direction) %>%
        mutate(cluster = factor(cluster, levels = which.clusters),
               direction = factor(direction, levels = c("upregulated", "downregulated"))) %>%
        group_by(cluster, direction) %>%
        summarise(n_sig_genes = n(), .groups = "drop") %>%
        complete(cluster, direction)
    if (is.null(x.lim)) {
        x.lim <- Barplot %>% group_by(cluster) %>% summarise(m = sum(n_sig_genes)) %>% pull(m) %>% max()
        x.lim <- round(x.lim + (x.lim * 0.05))
    }
    Barplot <- Barplot %>% ggplot(aes(x = cluster, y = n_sig_genes, fill = direction)) +
        geom_col() +
        scale_fill_manual(values = c(cb_red, cb_blue)) +
        scale_y_continuous(limits = c(0, x.lim)) +
        theme_bw() +
        theme(axis.text.x = element_text(angle = 90, vjust = .5, hjust = 1))
    return(Barplot)
}

display_venn <- function(x, ...){
  library(VennDiagram)
  grid.newpage()
  venn_object <- venn.diagram(x, filename = NULL, ...)
  grid.draw(venn_object)
}

correct_sign <- function(object){
  sign = sign(median(object$rotation[,1]))
  if(sign < 0) {
    object$rotation[,1] <- -1*object$rotation[,1]
    object$x[,1] <- -1*object$x[,1]
  }
  return(object)
}

GetMarkers <- function(region){
    region <- as.character(region)
    CellType_genes <- mouseMarkerGenesCombined[[region]]

    for (i in 1:length(CellType_genes)) {
        CellType_genes[[i]] <- as.vector(mouse2human(CellType_genes[[i]])$humanGene)
    }
    
    names(CellType_genes) <- sapply(names(CellType_genes), function(x) paste(x, "Genes", sep="_"))
    
    # Exclude specific genes: 5HTR3A - also expressed in VIP negative 5HTR3A cells.
    if (region == "Cortex") {
        CellType_genes$GabaVIPReln_Genes <- CellType_genes$GabaVIPReln_Genes[!CellType_genes$GabaVIPReln_Genes %in% "HTR3A"]
    }
    
    names(CellType_genes) <- sapply(names(CellType_genes), function(x) gsub(" ", "_", x))
    return(CellType_genes)
} 

# Estimates MGPs for a cell type given an expression matrix and a set of markers.
# The expression matrix is samples in cols and genes in rows. Markers need to
# be on rownames. Matrix of expression should be logCPM.
# If only a subset of samples (columns) are to be used for the estimation,
# include the column in subset.samples argument. A typical case is where you
# only want to estimate the cell types using the controls. The estimations will
# be carried out for everyone, but selection of markers will be only done using
# the subset. The default value (NULL) does not subset.
# mc.cores : sets the number of parallel cores (default = 1)
# n.permutations : sets number of bootstraps (default = 100)
MGP <- function(exprMatrix, markers, subset.samples = NULL, mc.cores = 1, n.permutations = 100) {
    if (!any(markers %in% rownames(exprMatrix))) stop("No markers found in rownames of expression matrix")
    if (!is.matrix(exprMatrix)) stop("exprMatrix argument needs to be a numeric matrix")
    if (is.null(subset.samples)) subset.samples = 1:ncol(exprMatrix)
    exprMatrix.s <- exprMatrix[rownames(exprMatrix) %in% markers,subset.samples]
    exprMatrix.s <- exprMatrix.s[,which(apply(exprMatrix.s, 2, var) != 0)]
    if (ncol(exprMatrix.s) < 3) stop("At least 3 samples are needed in the expression matrix")
    if (nrow(exprMatrix.s) < 2) {
        warning("Not enough markers found in expression matrix. Returning NULL")
        return(NULL)
    }

    # Bootstrap without replacement the samples (80%)
    Nb <- max(3,round(0.8*ncol(exprMatrix.s)))
    PCAresults <- lapply(1:n.permutations, function(i){
        exprSubset <- exprMatrix.s[,sample(1:ncol(exprMatrix.s), Nb, replace = FALSE)]
        exprSubset <- exprSubset[which(apply(exprSubset, 1, var) != 0),]
        pca.tmp <- prcomp(t(exprSubset), scale = TRUE) %>%
            correct_sign
    })
    rots.tmp <- lapply(1:length(PCAresults), function(i) {
            df.tmp <- data.frame(names(PCAresults[[i]]$rotation[,1]), PCAresults[[i]]$rotation[,1])
            names(df.tmp) <- c("marker", paste0("rot_", i))
            return(df.tmp)
    })  %>% purrr::reduce(full_join, by = "marker") %>%
        as_tibble %>%
        gather(bootstrap, value, starts_with('rot')) %>%
        na.omit
    # Remove negative markers
    rots <- rots.tmp %>%  group_by(marker) %>%
        summarise(Variance = var(value), Median = median(value)) %>%
        ungroup %>%
        filter(Median > 0)
    qs.median <- quantile(rots$Median, c(.25,.75))
    iqr.median.rots <- qs.median[2] - qs.median[1]
    fences.median <- c(qs.median[1] - 1.5 * iqr.median.rots,
                qs.median[2] + 1.5 * iqr.median.rots)
    rots <- rots %>% mutate(Median.outlier = ifelse(Median < fences.median[1] | Median > fences.median[2], TRUE, FALSE))
    qs.var <- quantile(rots$Variance, c(.25,.75))
    iqr.var.rots <- qs.var[2] - qs.var[1]
    fences.var <- c(qs.var[1] - 1.5 * iqr.var.rots,
                qs.var[2] + 1.5 * iqr.var.rots)
    rots <- rots %>% mutate(Variance.outlier = ifelse(Variance < fences.var[1] | Variance > fences.var[2], TRUE, FALSE))
    rots <- rots %>% filter((!Median.outlier) & (!Variance.outlier))
    selected.markers <- rots$marker

    # New bootstrap without replacement the samples (80%) with kept markers
    exprMatrix.s <- exprMatrix[rownames(exprMatrix) %in% selected.markers,]

    Nb <- max(3,round(0.8*ncol(exprMatrix.s)))
    PCAresults <- mclapply(1:n.permutations, function(i){
        exprSubset <- exprMatrix.s[,sample(1:ncol(exprMatrix.s), Nb, replace = FALSE)]
        exprSubset <- exprSubset[which(apply(exprSubset, 1, var) != 0),]
        pca.tmp <- prcomp(t(exprSubset), scale = TRUE) %>%
            correct_sign
    }, mc.cores = mc.cores)

    mgp <- lapply(1:length(PCAresults), function(i) {
            df.tmp <- data.frame(names(PCAresults[[i]]$x[,1]), PCAresults[[i]]$x[,1])
            names(df.tmp) <- c("sample_id", paste0("rot_", i))
            return(df.tmp)
    })  %>% purrr::reduce(full_join, by = "sample_id") %>%
        gather(bootstrap, value, starts_with('rot')) %>%
        na.omit %>%
        group_by(sample_id) %>%
        summarise(Median = median(value))

    return(mgp)
}

dummify <- function(m, reduce.binary=FALSE) {
    factor.cols <- sapply(m, function(x) is.factor(x) || is.character(x) || is.logical(x))
    if (sum(factor.cols) == 0) return(m)
    d <- lapply(colnames(m)[factor.cols], function(col.name) {
               mm <- model.matrix.lm(~.+0, as.data.frame(m[,col.name, drop=FALSE]), na.action=na.pass)
               if (ncol(mm)==2 && reduce.binary==TRUE) {
                   mm <- mm[,1,drop=FALSE]
               }
               return(mm)
    }) %>% do.call('cbind', .)
    return(cbind(m[!factor.cols], d))
}

assocVars <- function(M, id.col=1, method=c("pearson", "kendall", "spearman"), use="everything", reduce.binary=FALSE,
                      pvalues=FALSE, adjust="holm", alpha=.05) {
    # Test variance
    M <- as.data.frame(M) %>% mutate_if(is.character, as.factor)
    # Get IDs
    ids <- M[[id.col]]
    M <- M[-id.col]
    # Dummify
    m <- dummify(M, reduce.binary=reduce.binary)
    m <- as.data.frame(m)
    zeroVar <- (apply(mutate_if(M, is.factor, as.integer), 2, var, na.rm=TRUE) == 0)
    apply(mutate_if(M, is.factor, as.integer), 2, sd, na.rm=TRUE)
    if (any(zeroVar)) stop("Variables ", paste(colnames(M)[zeroVar]), " have zero variance")
    rownames(m) <- ids
    if (pvalues == TRUE) {
        cor.mat <- psych::corr.test(m, method=method, use=use, adjust=adjust, alpha=alpha)$p
    } else {
        cor.mat <- cor(m, method=method, use=use)
    }
    diag(cor.mat) <- NA
    return(cor.mat)
}

# mult.test.corr corrects p.values. Options are "none" (if no correction),
# "all" (for correcting all p-values), "each" (for correcting in each PC)
assocPCA <- function(M, variables, center=TRUE, scale=TRUE, ncomp=4, mult.test.corr="none", mult.test.method = "BH",
                     plot.return=FALSE, plot.alpha=0.05, plot.font.size=3, plot.colour="#c70039") {
    if (!mult.test.corr %in% c("none", "all", "each")) stop("mult.test.corr must be one of 'none', 'all', 'each'")
    keep <- apply(M, 2, var)!=0
    M <- M[,keep]
    pca <- prcomp(M, center=center, scale.=scale)
    ncomp <- min(ncomp, dim(pca$x)[2])
    var.exp <- summary(pca)$importance[2,1:ncomp]
    var.exp.str <- paste0(formatC(round(var.exp*100)), '%')
    res <- sapply(1:ncomp, function(i) {
                  unlist(lapply(1:ncol(variables), function(j) {
                             kept <- !is.na(variables[,j])
                             vars <- model.matrix(~.-1, variables[kept,j, drop=FALSE])
                             if (ncol(vars) == 2){vars <- vars[,1,drop=FALSE]}
                             ret <- sapply(1:ncol(vars), function(jp) {
                                        if (var(vars[,jp]) == 0) return(NA)
                                        summary(lm(pca$x[kept,i] ~ vars[,jp]))$coefficients[2,4]
                                    })
                             names(ret) <- colnames(vars)
                             ret
                  }))
          })
    colnames(res) <- paste0('PC', seq(1:ncomp), ' (', var.exp.str, ')')
    if (mult.test.corr=="none") {
        res <- rbind(res,var.exp)
    } else if (mult.test.corr=="all") {
        Colnames <- colnames(res)
        Rownames <- rownames(res)
        Dims <- dim(res)
        res <- p.adjust(res, method = mult.test.method)
        dim(res) <- Dims
        colnames(res) <- Colnames
        rownames(res) <- Rownames
        res <- rbind(res,var.exp)
    } else {
        Colnames <- colnames(res)
        Rownames <- rownames(res)
        res <- apply(res, 2, p.adjust, method = mult.test.method)
        colnames(res) <- Colnames
        rownames(res) <- Rownames
        res <- rbind(res,var.exp)
    }
    if (plot.return==TRUE) {
        Data <- res %>% as.data.frame %>% rownames_to_column("variable") %>%
            gather(key='PC', value='p.value', -variable) %>%
            mutate(PC=factor(as.character(PC))) %>%
            mutate(sig=ifelse(p.value<plot.alpha, "SIG", "NOTSIG"), log10p=-log10(p.value)) %>% 
            #mutate(p.lab=ifelse(p.value<plot.alpha, format(p.value, digits=2), NA))
            mutate(p.lab = formatC(p.value, format = "g", flag = "#", digits= 2))
        Pcs <- levels(as.factor(Data$PC))
        Levels <- Pcs[order(as.integer(gsub("PC", "", gsub(" \\(.+\\)", "", Pcs))))]
        Data$PC <- factor(Data$PC, levels=Levels)
        #Data$variable <- factor(Data$variable, levels=rev(colnames(variables)))
        ggplot(data=Data %>% filter(variable!="var.exp"), aes(x=PC, y=variable)) +
            geom_tile(aes(alpha=log10p, fill=sig), colour="white") +
            geom_text(aes(label=p.lab), size=plot.font.size) +
            scale_fill_manual(values=c(SIG=plot.colour, NOTSIG="black"), guide="none") +
            scale_alpha(guide="none", name="-log10(p-value)") +
            scale_x_discrete(position="top", expand=c(0,0)) +
            scale_y_discrete(expand=c(0,0)) + 
            theme_classic() +
            theme(axis.title.y=element_blank(),
                  axis.title.x=element_blank(),
                  axis.ticks=element_blank(),
                  axis.line=element_blank()) -> Plot
            return(Plot)
    } else {
        return(res)
    }
}

stars.pvalue <- function(pval) {
    stars <- rep('', length(pval))
    stars[pval<=0.1] <- "."
    stars[pval<=0.05] <- "*"
    stars[pval<=0.01] <- "**"
    stars[pval<=0.001] <- "***"
    return(stars)
}

confint.rlm <- function(object, ...){
  object$df.residual <- MASS:::summary.rlm(object)$df[2]
  confint.lm(object, ...)
}

tukey_outliers <- function(x, k=1.5, direction=FALSE, na.rm=FALSE) {
    x.len <- length(x)
    if (na.rm) {
        x.se.na <- is.na(x)
        which.not.na <- which(!is.na(x))
        x <- x[which.not.na]
    }
    qs <- quantile(x, c(.25,.75))
    iqr <- qs[2] - qs[1]
    fences <- c(qs[1] - k * iqr,
                qs[2] + k * iqr)
    if (direction) {
        outliers <- ifelse(x<fences[1], -1, ifelse(x>fences[2], 1, 0))
    } else {
        outliers <- (x<fences[1] | x>fences[2])
    }
    if (na.rm){
        final_outliers <- rep(NA, x.len)
        j <- 1
        for (i in 1:x.len){
            if (!x.se.na[i]) {
                final_outliers[i] <- outliers[j]
                j <- j + 1
            }
        }
        return(final_outliers)
    } else {
        return(outliers)
    }
}

scale2 <- function(x, na.rm=FALSE) (x - mean(x, na.rm = na.rm)) / sd(x, na.rm)

