Code for the manuscript "Single-nucleus transcriptomics reveals disease- and pathology-specific signatures in alpha-synucleinopathies" (Nido et al. 2023)
